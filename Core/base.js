import Pulse from 'pulse-framework';

import bookmark from './collections/bookmark';

export default new Pulse({
  collections: {
    bookmark
  },
  data: {
    test: false
  }
})


