import os from 'os'
const userHomeDir = os.homedir();

// import fs from 'fs';

export default({
  data: {
    browsers: {
      chrome: {
        location: {
          windows: `C:\\Users\\${userHomeDir}\\AppData\\Local\\Google\\Chrome\\User Data\\Default\\Bookmarks`,
          macos: `/Users/${userHomeDir}/Library/ApplicationSupport/Google/Chrome/Bookmarks`
        }
      },
      brave: {
        location: {
          windows: `C:\\Users\\${userHomeDir}\\AppData\\Local\\BraveSoftware\\Brave-Browser\\User Data\\Default\\Bookmarks`,
          macos: `/Users/${userHomeDir}/Library/ApplicationSupport/BraveSoftware/Brave-Browser/Default/Bookmarks`
        }
      }
    },
     cache: null
  },
  actions: {
    import({ bookmark }, browserType) {
      var osType = null;
      switch(os.type()) {
        case 'Darwin':
          osType = 'macos'
          break;
        case 'Windows_NT':
            osType = 'windows'
          break;
      }

      // fs.readFile(bookmark.browsers[browserType].location[osType]);
      console.log(bookmark.browsers[browserType].location.[osType])

      // console.log(JSON.parse(bookmark.browsers[browserType].location[osType]))
    }

  }
})